#!/bin/bash

# Set your ServiceNow credentials and instance URL
SERVICE_NOW_INSTANCE=$SK_SERVICENOW_INSTANCE_FULL
USERNAME=$SK_SERVICENOW_USERNAME
PASSWORD=$SK_SERVICENOW_PASSWORD
CHANGE_REQUEST_ID=$1
jobID=$2


# Set the ServiceNow API endpoint for the change request
SERVICE_NOW_API="https://${SERVICE_NOW_INSTANCE}/api/now/table/change_request/${CHANGE_REQUEST_ID}"

# Update the JOB ID cURL
curl --request PUT \
     --url "${SERVICE_NOW_API}" \
     --user "${USERNAME}:${PASSWORD}" \
     --header "Accept: application/json" \
     --header "Content-Type: application/json" \
     --data "{
        \"u_job_id\": \"${jobID}\"
     }"
