#!/bin/bash

# Set variables
MR_IID=$1
PROJECT_ID=$CI_PROJECT_ID
ACCESS_TOKEN=$SK_CI_JOB_TOKEN
SERVICE_NOW_INSTANCE=$SK_SERVICENOW_INSTANCE_FULL
USERNAME=$SK_SERVICENOW_USERNAME
PASSWORD=$SK_SERVICENOW_PASSWORD
CHANGE_REQUEST_ID=$2

# Check if the job failed
if [ "$CI_JOB_STATUS" == "failed" ]; then
  # Add a comment to the merge request
 
  COMMENT=":x: CI/CD Job Failed: Job ($CI_JOB_NAME) failed! JOB ID $CI_JOB_ID . "
  curl --request POST --header "PRIVATE-TOKEN: $ACCESS_TOKEN" --data "body=$COMMENT" "https://gitlab.com/api/v4/projects/$PROJECT_ID/merge_requests/$MR_IID/notes"


 # Create a dynamic link to the GitLab merge request
  PROJECT_URL="https://gitlab.com/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME"
  MR_LINK="[code]<a href=\"$PROJECT_URL/merge_requests/$MR_IID\" target=\"_blank\">Link to GitLab Merge Request</a>[/code]"
  
  #Create the comment message
  COMMENT_MESSAGE="❌ GitLab CI/CD Pipeline Failed: Job ($CI_JOB_NAME) failed! JOB ID $CI_JOB_ID :: $MR_LINK "
  
  # Construct the JSON payload using jq
  JSON_PAYLOAD=$(jq -n --arg comment "$COMMENT_MESSAGE" '{"work_notes": $comment}')

  SERVICE_NOW_API="https://${SERVICE_NOW_INSTANCE}/api/now/table/change_request/$CHANGE_REQUEST_ID"

# Send the API request with the properly formatted JSON payload
  curl --request PUT \
    --url "${SERVICE_NOW_API}" \
    --user "${USERNAME}:${PASSWORD}" \
    --header "Accept: application/json" \
    --header "Content-Type: application/json" \
    --data "$JSON_PAYLOAD"
fi
